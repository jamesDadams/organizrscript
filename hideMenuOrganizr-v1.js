var burgerButton = document.createElement('i');
burgerButton.style.position = "absolute";
burgerButton.style.right = "20px";
burgerButton.style.bottom = "10px";
burgerButton.className = "fa fa-bars";
burgerButton.setAttribute("onclick", "changeStateMenu()");

var divIframe = document.getElementsByClassName("iFrame-listing p-0 show");
divIframe[0].appendChild(burgerButton)


function changeStateMenu(){
        var margin = document.getElementsByClassName("iFrame-listing p-0 show");
        var navigationTab = document.querySelectorAll('[role="navigation"]');
        var valueMargin = getComputedStyle(margin[0]).getPropertyValue('margin-Left');
        
        if(valueMargin == "60px"){
            navigationTab[0].style.display="none";
            margin[0].style.marginLeft="0";
        }else{
            navigationTab[0].style.display="unset";
            margin[0].style.marginLeft="60px";
        }
}
